package id.ac.ub.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.ac.ub.room.recycler.MahasiswaAdapter;
import id.ac.ub.room.recycler.Mahasiswa;

public class MainActivity extends AppCompatActivity {
    Button bt1, bt2;
    EditText et1, et2;
    RecyclerView recyclerView;
    MahasiswaAdapter mahasiswaAdapter;
    private AppDatabase appDb;
    ArrayList<Mahasiswa> mahasiswaArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!et1.getText().toString().equals("") || !et1.getText().toString().equals("")) {
                    Item item = new Item();
                    item.setNama(et1.getText().toString());
                    item.setNim(et2.getText().toString());
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            appDb.itemDao().insertAll(item);
                        }
                    });
                }
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Item> list = (ArrayList<Item>) appDb.itemDao().getAll();
                        int jmlDataDb = list.size();
                        int jmlDataArr = mahasiswaArrayList.size();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(jmlDataDb != jmlDataArr){
                                    mahasiswaArrayList.removeAll(mahasiswaArrayList);
                                }
                                for (Item item : list) {
                                    Random ran = new Random();
                                    int choose = ran.nextInt(3);
                                    Mahasiswa mahasiswa = new Mahasiswa();
                                    mahasiswa.setNama(item.getNama());
                                    mahasiswa.setNim(item.getNim());
                                    if(jmlDataDb != jmlDataArr){
                                        mahasiswaArrayList.add(mahasiswa);
                                    }
                                }

                                initRecyclerView(mahasiswaArrayList);
                            }
                        });
                    }
                });
            }
        });

    }

    private void initRecyclerView(ArrayList<Mahasiswa> arrayList){
        recyclerView = findViewById(R.id.rv01);
        mahasiswaAdapter = new MahasiswaAdapter(arrayList, this);
        recyclerView.setAdapter(mahasiswaAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
