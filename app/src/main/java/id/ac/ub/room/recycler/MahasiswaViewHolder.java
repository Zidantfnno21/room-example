package id.ac.ub.room.recycler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import id.ac.ub.room.R;


public class MahasiswaViewHolder extends RecyclerView.ViewHolder {

    TextView tv_nama, tv_nim;
    ImageView iv_foto;
    Context context;


    public MahasiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        context = itemView.getContext();
        tv_nama = itemView.findViewById(R.id.tvNama);
        tv_nim = itemView.findViewById(R.id.tvNim);

    }

    public TextView getTv_nama() {
        return tv_nama;
    }

    public TextView getTv_nim() {
        return tv_nim;
    }



    public Context getContext() {
        return context;
    }
}
