package id.ac.ub.room.recycler;

public class Mahasiswa {
    String nama;
    String nim;

    public String getNama() {
        return nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
}

